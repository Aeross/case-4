package com.brightcove.playvideos;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.brightcove.player.edge.Catalog;
import com.brightcove.player.edge.PlaylistListener;
import com.brightcove.player.event.EventEmitter;
import com.brightcove.player.mediacontroller.BrightcoveMediaController;
import com.brightcove.player.model.Playlist;
import com.brightcove.player.view.BaseVideoView;
import com.brightcove.player.view.BrightcoveExoPlayerVideoView;
import com.brightcove.player.view.BrightcovePlayer;

public class MainActivity extends BrightcovePlayer {
    public static final String FONT_AWESOME = "fontawesome-webfont.ttf";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        brightcoveVideoView = (BrightcoveExoPlayerVideoView) findViewById(R.id.brightcove_video_view);


        // Get the event emitter from the SDK and create a catalog request to fetch a video from the
        // Brightcove Edge service, given a video id, an account id and a policy key.
        EventEmitter eventEmitter = brightcoveVideoView.getEventEmitter();
        //Set the new media controls
        brightcoveVideoView.setMediaController(new BrightcoveMediaController(brightcoveVideoView, R.layout.default_media_controller));
        //Init the like button behavior
        initButtons(brightcoveVideoView);

        Catalog catalog = new Catalog(eventEmitter, getString(R.string.account), getString(R.string.policy));
        catalog.findPlaylistByID("6031817847001", new PlaylistListener() {
            @Override
            public void onPlaylist(Playlist playlist) {
                brightcoveVideoView.addAll(playlist.getVideos());
                brightcoveVideoView.start();
            }

            @Override
            public void onError(String s) {
                throw new RuntimeException(s);
            }
        });
    }

    private void initButtons (final BaseVideoView brightcoveVideoView) {
        Typeface font = Typeface.createFromAsset(this.getAssets(), FONT_AWESOME);

        Button thumbsUp = (Button) brightcoveVideoView.findViewById(R.id.thumbs_up);
        if (thumbsUp != null) {
            // By setting this type face, we can use the symbols(icons) present in the font awesome file
            thumbsUp.setTypeface(font);
        }
        thumbsUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast likeToast = Toast.makeText(MainActivity.this, "Like!", Toast.LENGTH_SHORT);
                likeToast.setGravity(Gravity.TOP, 0, 60);
                likeToast.show();

            }
        });
    }
}
